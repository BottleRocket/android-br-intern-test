### BR Intern Test

Welcome!

This is a test to see if you have what it takes to become an intern at Bottle Rocket Studios!

The project is a list of Near Earth Objects (NEOs). With it we will be able to get a good quick overview of some general data about them.

The project is about 90% complete, we just need you come in and finish it up! 

#### App's architecture

This app uses Kotlin with an MVVM Architecture and Data Binding to keep the code clean and simple. It is built with Android Studio 4.0, so please be sure you have AS 4.0 or later to compile.

Here are some links to help you out:

* [Android Studio](https://developer.android.com/studio)
* [Kotlin](https://kotlinlang.org/)
* [MVVM](https://developer.android.com/jetpack/docs/guide)
* [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
* [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
* [Data Binding](https://developer.android.com/topic/libraries/data-binding)

#### Instructions

This app has 10 TODOs to complete and 4+ Bonus TODOs to optionally complete. To find out where all the TODOs are located, click the TODO tab at the bottom of Android Studio

After all of the TODOs completed, you should have a simple app that will list out NEOs for this last week showing name, distance, size, speed, and date. Clicking a NEO should take you to a web page with more info about that particular one. 

##### Once completed
1. Clean the build (`Build -> Clean Project`) (helps reduce the size)
2. Zip or compress the project folder
3. Send it to (BR EMAIL)

#### TODOs
* Step 1: Get API Key (if you need it) from https://api.nasa.gov/
* Step 2: Set RecyclerView adapter to the RecyclerView
* Step 3: Set RecyclerView Data from nearEarthObjects LiveData in ViewModel
* Step 4: Sort the Near Earth Objects by date
* Step 5: Add a TextView for velocity under sizeView
* Step 6: Add a on click listener to call neoClicked in the ViewModel
* Step 7: Use the url from nearEarthObjectClicked LiveData in ViewModel to start a web intent, opening the url in the user's browser
* Step 8: Add Start and End Date Queries to get data for the last week, with end date being the current day
* Step 9: Fix a bug! After I swipe to refresh the data (drag down on the screen), the loading spinner stays up! Fix it so that the spinner disappears after a successful (or unsuccessful) refresh.
* Step 10: Let us know how the app could be improved (Code, UI, New Features, etc) (Bottom of README)
                
#### Bonus (Optional)

* Bonus: Change size to use meters diameter and velocity to use kilometers per hour. *Hint - Check the API Docs*
* Bonus: Give the distance background circle different colors based on distance. *Hint - Check colors.xml*
* Bonus: Add a ripple effect to the NearEarthObject Item when clicked
* Bonus: Update the App Icon

* Extra Bonus points if you want to:
    * Improve the code
    * Update the UI
    * Improve Documentation / Code Comments
    * Add Unit Tests

### Finished Product Screenshot (Including Bonus)
-------------
![Intern Screenshot](screenshots/near_earth_objects_intern_screenshot.png)

    
#### TODO Step 10
##### How the app could be improved (Code, UI, New Features, etc) - Write out your suggestions below

