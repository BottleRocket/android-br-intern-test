package com.bottlerocketstudios.android.brinterntest.network

import com.bottlerocketstudios.android.brinterntest.network.dtos.NearEarthObjectDto
import com.bottlerocketstudios.android.brinterntest.utils.getFormattedDtoDate
import com.bottlerocketstudios.android.brinterntest.utils.getWeekAgo
import com.bottlerocketstudios.android.brinterntest.utils.getToday
import retrofit2.Response

interface NearEarthObjectRepo {
    suspend fun getNearEarthObjects(): Response<NearEarthObjectDto>
}

class NearEarthObjectRepoImpl : NearEarthObjectRepo {
    //TODO Step 1: Get API Key (if you need it) from https://api.nasa.gov/
    // DEMO_KEY has 30 per hour / 50 per day limit
//    val API_KEY = "DEMO_KEY"
    val API_KEY = "LuSYSRZNqmJMjUVgtNpI0srV7Qy6qsgfrRqgxzPd"

    override suspend fun getNearEarthObjects(): Response<NearEarthObjectDto> {
        val start: String = getFormattedDtoDate(getToday())
        val end: String = getFormattedDtoDate(getWeekAgo())

        return NetworkUtils.getNASAEndpoints().getNeos(start, end, API_KEY)
    }

}