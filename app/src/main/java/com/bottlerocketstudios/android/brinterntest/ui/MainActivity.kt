package com.bottlerocketstudios.android.brinterntest.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bottlerocketstudios.android.brinterntest.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}