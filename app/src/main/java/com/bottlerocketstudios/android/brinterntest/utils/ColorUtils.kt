package com.bottlerocketstudios.android.brinterntest.utils

import com.bottlerocketstudios.android.brinterntest.R

//TODO Bonus 2: Give the distance background circle different colors based on distance
//  Hint - Check colors.xml
fun getColorBasedOnDistance(distance: Int): Int {
    return when {
        distance <= 10 -> R.color.distance0
        distance <= 20 -> R.color.distance10
        distance <= 30 -> R.color.distance20
        distance <= 40 -> R.color.distance30
        distance <= 50 -> R.color.distance40
        distance <= 60 -> R.color.distance50
        distance <= 70 -> R.color.distance60
        distance <= 80 -> R.color.distance70
        distance <= 90 -> R.color.distance80
        distance <= 100 -> R.color.distance90
        distance > 100 -> R.color.distance100
        else -> R.color.distance0
    }
}

fun getTextColorBasedOnSize(maxDiameter: Int): Int {
    return if (maxDiameter >= 2000) R.color.bigRed else R.color.defaultTextColor
}

fun getTextColorBasedOnVelocity(velocity: Int): Int {
    return if (velocity >= 50000) R.color.bigRed else R.color.defaultTextColor
}