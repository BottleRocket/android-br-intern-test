package com.bottlerocketstudios.android.brinterntest.ui.neolist

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bottlerocketstudios.android.brinterntest.R
import com.bottlerocketstudios.android.brinterntest.network.NearEarthObjectRepoImpl
import com.bottlerocketstudios.android.brinterntest.network.NetworkUtils
import kotlinx.android.synthetic.main.near_earth_objects_fragment.*

class NearEarthObjectsFragment : Fragment() {

    private val viewModel: NearEarthObjectsViewModel by lazy {
        NearEarthObjectsViewModel(
            NearEarthObjectRepoImpl()
        )
    }
    private val adapter: NearEarthObjectAdapter by lazy { NearEarthObjectAdapter(viewModel) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.near_earth_objects_fragment, container, false)
    }

    //TODO Step 9: Fix a bug! After I swipe to refresh the data (drag down on the screen),
    //  the loading spinner stays up! Fix it so that the spinner disappears after a successful
    //  (or unsuccessful) refresh.

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //TODO Step 2: Set RecyclerView adapter to the RecyclerView
        recyclerView.adapter = adapter

        //TODO Step 3: Set RecyclerView Data from nearEarthObjects LiveData in ViewModel
        viewModel.nearEarthObjects.observe(viewLifecycleOwner, Observer {
            swipeRefresh.isRefreshing = false
            adapter.setData(it)
        })

        viewModel.errorToast.observe(viewLifecycleOwner, Observer {
            swipeRefresh.isRefreshing = false
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        })

        //TODO Step 7: Use the url from nearEarthObjectClicked LiveData in ViewModel to start a web intent
        viewModel.nearEarthObjectClicked.observe(viewLifecycleOwner, Observer {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(it)))
        })

        swipeRefresh.setOnRefreshListener {
            getNearEarthObjectData()
        }

        getNearEarthObjectData()
    }

    private fun getNearEarthObjectData() {
        if (NetworkUtils.isConnected(requireContext())) viewModel.loadNearEarthObjects()
        else {
            swipeRefresh.isRefreshing = false
            Toast.makeText(requireContext(), getString(R.string.no_network_connection), Toast.LENGTH_LONG).show()
        }
    }

}