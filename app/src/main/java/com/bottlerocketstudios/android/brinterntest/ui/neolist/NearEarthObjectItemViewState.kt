package com.bottlerocketstudios.android.brinterntest.ui.neolist

import androidx.annotation.ColorRes

data class NearEarthObjectItemViewState(
    val name: String,
    val date: String,
    val distance: String,
    @ColorRes val distanceBackgroundColor: Int,
    val size: String,
    @ColorRes val sizeTextColor: Int,
    val velocity: String,
    @ColorRes val velocityTextColor: Int,
    val url: String
)