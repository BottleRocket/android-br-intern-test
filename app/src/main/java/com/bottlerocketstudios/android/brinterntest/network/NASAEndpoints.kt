package com.bottlerocketstudios.android.brinterntest.network

import com.bottlerocketstudios.android.brinterntest.network.dtos.NearEarthObjectDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

//TODO Step 8: Add Start and End Date Queries to get this last week
// API Documentation: https://api.nasa.gov/ - The feed date limit is 7 days.
interface NASAEndpoints {
    @GET("/neo/rest/v1/feed")
    suspend fun getNeos(
            @Query("start_date") startDate: String,
            @Query("end_date") endDate: String,
            @Query("api_key") apiKey: String
    ): Response<NearEarthObjectDto>
}