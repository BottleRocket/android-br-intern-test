package com.bottlerocketstudios.android.brinterntest.ui.neolist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bottlerocketstudios.android.brinterntest.network.NearEarthObjectRepo
import kotlinx.coroutines.launch

class NearEarthObjectsViewModel(private val nearEarthObjectRepo: NearEarthObjectRepo) : ViewModel() {
    private val _nearEarthObjects = MutableLiveData<List<NearEarthObjectItemViewState>>()
    val nearEarthObjects: LiveData<List<NearEarthObjectItemViewState>> = _nearEarthObjects

    private val _errorToast = MutableLiveData<String>()
    val errorToast: LiveData<String> = _errorToast

    private val _nearEarthObjectClicked = MutableLiveData<String>()
    val nearEarthObjectClicked: LiveData<String> = _nearEarthObjectClicked

    fun loadNearEarthObjects() {
        viewModelScope.launch {
            val neoCall = nearEarthObjectRepo.getNearEarthObjects()
            if (neoCall.isSuccessful) _nearEarthObjects.postValue(neoCall.body()?.toViewStates())
            else _errorToast.postValue(neoCall.message())
        }
    }

    fun nearEarthObjectClicked(url: String) {
        _nearEarthObjectClicked.value = url
    }
}