package com.bottlerocketstudios.android.brinterntest.network.dtos


import com.bottlerocketstudios.android.brinterntest.ui.neolist.NearEarthObjectItemViewState
import com.bottlerocketstudios.android.brinterntest.utils.getColorBasedOnDistance
import com.bottlerocketstudios.android.brinterntest.utils.getFormattedViewDate
import com.bottlerocketstudios.android.brinterntest.utils.getTextColorBasedOnSize
import com.bottlerocketstudios.android.brinterntest.utils.getTextColorBasedOnVelocity
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.math.roundToInt

@Serializable
data class NearEarthObjectDto(
    @SerialName("near_earth_objects") val neosListByDate: Map<String, List<NearEarthObject>> = mapOf()
) {

    @Serializable
    data class NearEarthObject(
        @SerialName("id") val id: String,
        @SerialName("name") val name: String,
        @SerialName("close_approach_data") val closeApproachData: List<CloseApproachData>,
        @SerialName("estimated_diameter") val estimatedDiameter: EstimatedDiameter,
        @SerialName("nasa_jpl_url") val nasaJplUrl: String,
        @SerialName("neo_reference_id") val neoReferenceId: String
    ) {
        @Serializable
        data class CloseApproachData(
            @SerialName("miss_distance") val missDistance: MissDistance,
            @SerialName("relative_velocity") val relativeVelocity: RelativeVelocity
        ) {
            @Serializable
            data class MissDistance(
                @SerialName("lunar") val lunar: String
            )

            @Serializable
            data class RelativeVelocity(
                @SerialName("miles_per_hour") val milesPerHour: String
            )
        }

        @Serializable
        data class EstimatedDiameter(
            @SerialName("feet") val feet: Feet
        ) {
            @Serializable
            data class Feet(
                @SerialName("estimated_diameter_max") val estimatedDiameterMax: Double,
                @SerialName("estimated_diameter_min") val estimatedDiameterMin: Double
            )
        }
    }

    //TODO Step 4: Sort the Near Earth Objects by date
    fun toViewStates(): List<NearEarthObjectItemViewState> {
        return neosListByDate.flatMap { hashMap ->
            hashMap.value.map { neo ->
                val distance = neo.closeApproachData[0].missDistance.lunar.toDouble().roundToInt()
                //TODO Bonus 1: Change size and velocity to be meters diameter and kilometers per hour
                //  Hint - Check the API Docs
                val size = neo.estimatedDiameter.feet.estimatedDiameterMax.toInt()
                val velocity = neo.closeApproachData[0].relativeVelocity.milesPerHour.toDouble().roundToInt()
                NearEarthObjectItemViewState(
                    name = neo.name,
                    date = getFormattedViewDate(hashMap.key),
                    distance = "$distance LD",
                    distanceBackgroundColor = getColorBasedOnDistance(distance),
                    size = "$size ft dia",
                    sizeTextColor = getTextColorBasedOnSize(size),
                    velocity = "$velocity mph",
                    velocityTextColor = getTextColorBasedOnVelocity(velocity),
                    url = neo.nasaJplUrl
                )
            }
        }.sortedBy { it.date }
    }
}
