package com.bottlerocketstudios.android.brinterntest.ui.neolist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bottlerocketstudios.android.brinterntest.R
import com.bottlerocketstudios.android.brinterntest.databinding.NearEarthObjectItemBinding
import com.bottlerocketstudios.android.brinterntest.ui.neolist.NearEarthObjectAdapter.MyViewHolder


class NearEarthObjectAdapter(private val neoViewModel: NearEarthObjectsViewModel) : RecyclerView.Adapter<MyViewHolder>() {

    private var neos: List<NearEarthObjectItemViewState> = emptyList()

    class MyViewHolder(val binding: NearEarthObjectItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding: NearEarthObjectItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.near_earth_object_item, parent, false
        )
        return MyViewHolder(binding)
    }

    override fun getItemCount() = neos.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.neoVM = neoViewModel
        holder.binding.neo = neos[position]
    }

    fun setData(neoList: List<NearEarthObjectItemViewState>) {
        neos = neoList
        notifyDataSetChanged()
    }
}
