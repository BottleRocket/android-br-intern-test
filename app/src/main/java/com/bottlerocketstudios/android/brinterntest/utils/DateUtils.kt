package com.bottlerocketstudios.android.brinterntest.utils

import java.text.SimpleDateFormat
import java.util.*

fun getToday(): Calendar = Calendar.getInstance()

fun getWeekAgo(): Calendar = Calendar.getInstance().apply { add(Calendar.DATE, -7) }

fun getFormattedDtoDate(calendar: Calendar): String {
    return SimpleDateFormat("yyyy-MM-dd", Locale.US).format(calendar.time)
}

fun getFormattedViewDate(dateString: String): String {
    val date = SimpleDateFormat("yyy-MM-dd", Locale.US).parse(dateString)
    return SimpleDateFormat("dd MMM yyyy", Locale.US).format(date)
}

